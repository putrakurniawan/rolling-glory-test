const jwt = require('../helpers/jwttoken');
const level = require('./level.middelware');
exports.auth = async (req, res, next) => {
    try {
        let token = req.headers['authorization'] || req.headers['x-access-token'];
        if (token.startsWith('Bearer ')) {
            token = token.slice(7, token.length);
        }
        const data = await jwt.decodeToken(token)
        req.userData = data;
        next();
    } catch (error) {
        console.log(error.message);
        console.log(error.stack);
        let objerror = {
            kode: 500,
            msg: "Token tidak valid. Silahkan login terlebih dahulu"
        }
        if (error.code) {
            objerror.msg = error.message;
            objerror.kode = error.code;
        }
        res.status(500).json(objerror)
    }
}