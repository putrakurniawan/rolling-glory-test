const modules = require('../libs/_module')

let result = {
    response_code:500,
    status: true,
    data: [],
    message: '',
}

exports.superAdminOnly = async(req, res, next)=>{
    try {
        if(req.userData.level === 'super admin'){
            next();
        }else{
            // console.log(req.userData.level);
            throw new Error('Not permissions');
        }
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
        modules.returnend(res, result);
    }
}

exports.global =  async(req,res,next)=>{
    try {
        // console.log(req.userData);
        if(['users','admins','super admin'].includes(req.userData.level)){
            next()
        }else{
            // console.log(req.userData.level);
            throw new Error('Not permissions')
        }
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
        modules.returnend(res, result);
    }
}

exports.admisglobal =  async(req, res, next)=>{
    try {
        if(['admins','super admin'].includes(req.userData.level)){
            next()
        }else{
            // console.log(req.userData.level);
            throw new Error('Not permissions')
        }
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
        modules.returnend(res, result);
    }
}