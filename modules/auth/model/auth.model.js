const DB = require('../../../public/knex-config')

exports.loginquery = async(data)=>{   
    const {username, password} = data
    const query = await DB('users').leftJoin('level','level.id','users.is_level').where({
      'users.username': username,
      'users.password': password
    }).select('users.id','users.name','users.id_users','level.level as level').then(rows => rows);
    var resultArray = Object.values(JSON.parse(JSON.stringify(query)))
    return resultArray;
}