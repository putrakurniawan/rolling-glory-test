const { it, expect, describe } = require("@jest/globals");
const { login } = require("./auth.controller");
const httpMocks = require('node-mocks-http');


describe('testLogin', () => {
    test('response', () => {
        const { response, request } = getHttp()
        expect(response._getStatusCode()).toBe(200)
    })

})


function getHttp() {
    const request = httpMocks.createRequest({
        method: 'POST',
        url: '/',
        body: {
            username: 'super admin',
            password: 'super admin'
        }
    });

    const response = httpMocks.createResponse();

    login(request, response, (err) => {
        expect(err).toBeFalsy();
    });

    return {
        response,
        request
    }
}
