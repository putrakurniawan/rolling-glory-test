const modules = require('../../../libs/_module')
const md5 = require('md5')
const {jwttoken } =  require('../../../helpers')
const logins = require('../model/auth.model')

exports.login = async (req, res) => {
    let result = {
        response_code:500,
        status: true,
        data: [],
        message: '',
    }
    try {
        const {username, password} = req.body
        let data = { username, password: md5(password) }
        const query = await  logins.loginquery(data);
        if (query.length === 0) {
            throw new Error('Username atau password salah')
        }
        let generateToken = await jwttoken.setToken(query[0])
        result.data = generateToken;
        result.status = true;
        result.response_code = 200;
    } catch (error) {
        console.log(error);
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
    }finally{
        modules.returnend(res, result);
    }
}

