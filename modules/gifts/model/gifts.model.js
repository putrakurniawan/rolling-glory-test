const DB = require('../../../public/knex-config');
const crypto = require('crypto');
const moment = require('moment'); 

exports.get_gifts = async (data) => {
    const { id_gifts, limit, page, sort, sort_rating } = data;
    const subquery = DB.raw('(SELECT ROUND(sum(rating)/ count(id_gifts), 1) from gifts_ratings where gifts_ratings.id_gifts = gifts.id_gifts) as rating')
    const query = DB('gifts').leftJoin('gifts_stock', 'gifts.id_gifts', 'gifts_stock.id_gifts')
    console.log(id_gifts);
    if (id_gifts) {
        query.where(function () {
            this.where('gifts.id_gifts', id_gifts)
        }).select('gifts.*', 'gifts_stock.stock', subquery)
    } else {
        query.select('gifts.id', 'gifts.name_gifts', subquery)
    }

    if (sort != null || sort_rating != null) {
        let giftsOrder;
        let ratingOrder;
        console.log(sort);
        if (sort == 'Terbaru') {
            orders = 'DESC'
            giftsOrder = { column: 'gifts.created_at', order: orders }
        } else if (sort == 'lama') {
            console.log('xxxx');
            orders = 'ASC'
            giftsOrder = { column: 'gifts.created_at', order: orders }
        }

        if (sort_rating == 'Terbesar') {
            ratings = 'DESC'
            ratingOrder = { column: 'rating', order: ratings }
        } else if (sort_rating == 'Terkecil') {
            ratings = 'ASC'
            ratingOrder = { column: 'rating', order: ratings }
        }

        query.orderBy([giftsOrder, ratingOrder])
    }

    if (limit) {
        query.limit(limit);
    }

    if (page) {
        query.offset((parseInt(page) - 1) * limit)
    }

    console.log(query.toString());
    return query
}

exports.insert_or_update_gifts = async (data, id_gifts_update) => {
    let column = id_gifts_update != undefined && id_gifts_update != null ? 'updated_at':'created_at';
    const {
        id_gifts,
        name_gifts,
        descriptioan_gifts,
        // category_gifts,
        id_user,
        is_available,
        is_best_seller,
        is_hot_item,
        img_gifts } = data

    const datas = {
        id_gifts: id_gifts,
        name_gifts: name_gifts,
        descriptioan_gifts: descriptioan_gifts,
        // category_gifts: category_gifts,
        created_by: id_user,
        is_available: is_available,
        is_best_seller: is_best_seller,
        is_hot_item: is_hot_item,
        img_gifts: img_gifts,
        [column]: moment().format('YYYY-MM-DD HH:mm:ss')
    }
    const querys = DB('gifts');
    if (!id_gifts_update) {
        querys.insert(datas)
    } else {
        querys.update(datas).where('id_gifts', id_gifts_update)
    }

    return querys
}

exports.insert_or_update_stok = async (data, id_stock_update) => {
    const {
        id_gifts,
        id_stok,
        stock,
    } = data

    const databody = {
        id_gifts: id_gifts,
        id_stock: id_stok,
        stock: stock,
    }
    const query = DB('gifts_stock');
    if (!id_stock_update) {
        query.insert(databody)
    } else {
        query.update(databody)
            .where('id_stock', id_stock_update)
    }

    return query
}

exports.query_get_stok = async (data) => {
    const { id_gifts } = data
    console.log(id_gifts);
    const query = DB('gifts_stock').where({
        id_gifts: id_gifts
    }).select('*')
    console.log(query.toString());
    return query;
}

exports.delete_gifts = async (data) => {
    const { id_gifts } = data
    const query = DB('gifts');
    return query.del().where({ id_gifts: id_gifts });
}

exports.insert_or_update_redeem = async (data) => {
    const { id_gifts, id_redeem, nomor_redeem, trx, id_users } = data
    let stockOut = DB('gifts_stock').update({
        stock: DB.raw('?? - 1', ['stock'])
    }).where({ id_gifts })
        .toString();
    await trx.raw(stockOut)
    let redeem = DB('gifts_redeem')
        .insert({
            id_redeem: id_redeem,
            id_gifts: id_gifts,
            nomor_redeem: nomor_redeem,
            id_users: id_users
        })
        .toString()
    await trx.raw(redeem);
    return true
}

exports.get_redeem = async (data) => {
    const { id_gifts, id_users } = data
    const query = DB('gifts_redeem');
    query.select('*')
    if (id_gifts) {
        query.where(function () {
            this.where('id_gifts', id_gifts)
        })
    }
    if (id_users) {
        query.where(function () {
            this.where('id_users', id_users)
        })
    }
    console.log(query.toString());
    return query
}

exports.get_rating = async (data) => {
    const { id_gifts, id_users } = data
    const query = DB('gifts_ratings');
    if (id_gifts) {
        query.where(function () {
            this.where('id_gifts', id_gifts)
        })
    }
    if (id_users) {
        query.where(function () {
            this.where('id_users', id_users)
        })
    }
    console.log(query.toString());
    return query
}

exports.insert_or_update_rating = async (data, id_ratings) => {
    const { id_gifts, id_users, trx, rating, id_rating } = data
    let datasrating
    if (id_ratings) {
        console.log('masuk kesini  ??? ');
        datasrating = DB('gifts_ratings')
            .update({ rating: rating })
            .where({id_rating: id_ratings })
            .toString();
    } else {
        datasrating = DB('gifts_ratings')
            .insert({
                id_gifts: id_gifts,
                rating: rating,
                id_users: id_users,
                id_rating: id_rating
            }).toString()
    }   
    await trx.raw(datasrating)
    return true
}
