const { it, expect, describe } = require("@jest/globals");
const { gifts_put_or_post } = require("./gifts.controller");
const httpMocks = require('node-mocks-http');


describe('test insert gifts', () => {
    test('type data check', () => {
        const { response, request } = insert();
        expect(request.body).toEqual({
            name_gifts: expect.any(String),
            descriptioan_gifts: expect.any(String),
            is_available: expect.any(String),
            is_best_seller: expect.any(String),
            is_hot_item: expect.any(String),
            img_gifts: expect.any(String),
            stock: expect.any(Number)
        })
    })
})

function insert() {
    const request = httpMocks.createRequest({
        method: 'POST',
        url: '/gifts',
        body: {
            name_gifts: 'Charger',
            descriptioan_gifts: 'charger',
            is_available: '1',
            is_best_seller: '0',
            is_hot_item: '0',
            img_gifts: 'tes.gift',
            stock: 10
        }
    });

    const response = httpMocks.createResponse();

    gifts_put_or_post(request, response, (err) => {
        expect(err).toBeFalsy();
    });

    return {
        response,
        request
    }
}

