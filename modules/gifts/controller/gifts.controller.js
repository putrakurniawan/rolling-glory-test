const modules = require('../../../libs/_module')
const gifts = require('../model/gifts.model')
const crypto = require('crypto')
const DB = require('../../../public/knex-config')
let result = {
    response_code: 500,
    status: true,
    data: [],
    message: '',
}

exports.gifts_get = async (req, res) => {
    try {
        const { page, limit, id_gifts, sort, sort_rating } = req.query;
        let data = { id_gifts, page, limit, sort, sort_rating }
        const query = await gifts.get_gifts(data)
        let hasil_map = query.map(x => {
            return {
                id: x.id,
                ...id_gifts !== undefined && id_gifts != null ? {
                    id_gifts: x.id_gifts,
                    name_gifts: x.name_gifts,
                    descriptioan_gifts: x.descriptioan_gifts,
                    category_gifts: x.category_gifts,
                    is_available: x.is_availabl,
                    is_best_seller: x.is_best_seller,
                    is_hot_item: x.is_hot_item,
                    img_gifts: x.img_gifts,
                    created_at: x.created_at,
                } : {},
                rating: Math.round(x.rating * 2) / 2
            }
        })
        result.data = hasil_map
        result.status = true;
        result.response_code = 200;
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
    } finally {
        modules.returnend(res, result);
    }
}

exports.gifts_put_or_post = async (req, res, next) => {
    try {

        switch (req.params.id_gifts) {
            case undefined:
                const idgiftsnew = `RGT-${crypto.randomBytes(16).toString('hex')}`;
                const idstoknew = `ST-${crypto.randomBytes(17).toString('hex')}`;
                let data = { id_gifts: idgiftsnew, id_user: req.userData.id_users, ...req.body }
                await gifts.insert_or_update_gifts(data);
                let dataStok = { id_stok: idstoknew, id_gifts: idgiftsnew, stock: req.body.stock }
                await gifts.insert_or_update_stok(dataStok);
                result.message = 'Insert Successfuly'
                break;

            default:
                const { id_gifts } = req.params
                const {
                    name_gifts,
                    descriptioan_gifts,
                    created_by,
                    is_available,
                    is_best_seller,
                    is_hot_item,
                    img_gifts,
                    stock
                } = req.body

                let datas = {
                    name_gifts,
                    descriptioan_gifts,
                    created_by,
                    is_available,
                    is_best_seller,
                    is_hot_item,
                    img_gifts,
                    stock
                }

                await gifts.insert_or_update_gifts(datas, id_gifts);
                let querystock = await gifts.query_get_stok({ id_gifts: id_gifts });
                let datastock, idstoknews;
                if (querystock.length > 0) {
                    idstoknews = querystock[0].id_stock;
                    datastock = { stock: stock };
                    await gifts.insert_or_update_stok(datastock, idstoknews);
                    result.message = 'Update Successfuly'
                } else {
                    result.message = 'Invalid data'
                }


                break;
        }

        result.status = true;
        result.response_code = 200;
        modules.returnend(res, result);

    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
        modules.returnend(res, result);
    }
}

exports.deleteData = async (req, res, next) => {
    try {
        const { id_gifts } = req.params
        let data = { id_gifts }
        await gifts.delete_gifts(data);

        result.message = 'Delete Successfuly'
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
    } finally {
        modules.returnend(res, result);
    }
}

exports.insert_or_update_redeem_or_rating = async (req, res, next) => {
    try {
        const { id_gifts, type_act } = req.params;
        const id_users = req.userData.id_users
        const trx = await DB.transaction();
        switch (req.params.type_act) {
            case 'redeem':
                let data = { id_gifts }
                const query = await gifts.get_gifts(data)
                if (query.length > 0) {
                    if (query[0].is_available === '0' || query[0].stock <= 0) {
                        throw new Error("stock sudah habis, silahkan memilih item yang lain")
                    }

                    const id_redeem = `RDM-${crypto.randomBytes(16).toString('hex')}`;
                    const nomor_redeem = `${crypto.randomBytes(20).toString('hex')}`;

                    let data = {
                        id_gifts,
                        id_redeem,
                        nomor_redeem,
                        trx,
                        id_users
                    }
                    await gifts.insert_or_update_redeem(data);
                    await trx.commit();
                    
                    result.message ='Succesfuly Redeem'
                } else {
                    await trx.rollback();
                    throw new Error("Data gifts tidak ditemukan, silihkan Muat ulang ")
                }
                break;
            default:
                let datas = {id_gifts,id_users}
                const id_rating = `RT-${crypto.randomBytes(5).toString('hex')}`
                const querygetredeem =await  gifts.get_redeem(datas)
                let datainsert = {
                    id_gifts,
                    id_users,
                    id_rating,
                    trx,
                    rating:req.body.rating
                }
                if(querygetredeem.length > 0){
                    const cekrating = await gifts.get_rating(datas)
                    console.log('xxxxxaaa:',cekrating);
                    if(cekrating.length > 0){
                        await gifts.insert_or_update_rating(datainsert, cekrating[0].id_rating);
                        result.message ='Succesfuly Rating update'
                    }else{
                        await gifts.insert_or_update_rating(datainsert)
                        result.message ='Succesfuly Rating insert'
                    }
                    await trx.commit();
                }else {
                    await trx.rollback();
                    throw new Error("Data gifts belum di redeem. Tidak dapat memberikan ratings")
                }
                break;
        }
        result.status = true;
        result.response_code = 200;
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
    } finally {
        modules.returnend(res, result);
    }

}


