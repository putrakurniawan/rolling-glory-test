const DB = require('../../../public/knex-config');
const moment = require('moment'); 


exports.get_user = async(data)=>{
    const {id_users, page, limit} = data;
    const query = DB('users').leftJoin('level','level.id','users.is_level');
    if(id_users){
        query.where({id_users}).select('users.id_users','users.name','level.name as level');
    }else{
        query.select('users.id_users','users.name','users.email','users.username','level.name as level')
    }
    if(limit){
        query.limit(limit);
    }
    if(page){
        query.offset((parseInt(page) - 1)* limit);
    }
    return query
}

exports.insert_or_update_user = async(data, id_user_update) => {
    let column = id_user_update != undefined && id_user_update != null ? 'updated_at':'created_at';
    const {
        id_users,
        email,
        username,
        name,
        password,
        is_level
    } = data

    const datas = {
        id_users:id_users,
        email:email,
        username:username,
        name:name,
        password:password,
        is_level:is_level,
        [column]: moment().format('YYYY-MM-DD HH:mm:ss')
    }

    const query = DB('users');
    !id_user_update ? query.insert(datas) : query.update(datas).where('id_users', id_user_update)
    
    return query
}

exports.delete_users = async(data) => {
    const {id_users} = data;
    const query = DB('users');
    return query.del().where({id_users: id_users});
}

