const modules = require('../../../libs/_module')
const users = require('../model/users.model')
const crypto = require('crypto')
const DB = require('../../../public/knex-config')
const md5 = require('md5')
let result = {
    response_code: 500,
    status: true,
    data: [],
    message: '',
}

exports.users_get = async (req, res) => {
    try {
        const { page, limit, id_users } = req.query;
        let data = { id_users, page, limit }
        const query = await users.get_user(data)

        result.data = query
        result.status = true;
        result.response_code = 200;
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
    } finally {
        modules.returnend(res, result);
    }
}

exports.user_post_or_put = async (req, res) => {
    try {

        switch (req.params.id_users) {
            case undefined:
                const idusersnew = `${crypto.randomBytes(16).toString('hex')}`;
                let data = { id_users: idusersnew, ...req.body }
                await users.insert_or_update_user(data);

                result.message = 'Insert Successfuly'
                break;

            default:
                const { id_users } = req.params
                const {
                    email,
                    username,
                    name,
                    password,
                    is_level
                } = req.body

                let datas = {
                    email,
                    username,
                    name,
                    password,
                    is_level
                }
                await users.insert_or_update_user(datas, id_users);
                result.message = 'Update Successfuly'

                break;
        }

        result.status = true;
        result.response_code = 200;
        modules.returnend(res, result);

    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
        modules.returnend(res, result);
    }
}

exports.deletedata = async (req, res) => {
    try {
        const { id_users } = req.params
        let data = { id_users }
        await users.delete_users(data);
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
    } finally {
        modules.returnend(res, result);
    }
}

exports.register = async (req, res) => {
    try {
        const idusersnew = `${crypto.randomBytes(16).toString('hex')}`;
        let level = 3;
        let data = { id_users: idusersnew,is_level: level, ...req.body }
        await users.insert_or_update_user(data);
        result.message = 'Register Successfuly'
        result.status = true;
        result.response_code = 200;
        
    } catch (error) {
        result.status = false;
        result.response_code = 500;
        result.message = error.toString();
    } finally {
        modules.returnend(res, result);
    }
}