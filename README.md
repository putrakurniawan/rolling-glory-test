### Note: Password untuk login untuk admin adalah `u: super admin, p: super admin`
## 1.a Instalasi
1. Lakukan clone pada repo ini.
2. Pastikan sudah menginstall Database Mysql pada sistem.
3. Jalankan aplikasi database Mysql.
4. Exports terlebih dahulu file skema databasenya pada folder `./docs` dengan nama file `test-rolling.sql`
5. Setting database postgresql dengan username dan password sesuai dengan database local Anda.
6. Jalankan `npm install`

## 2.a Menjalankan server secara local
1. Jika sudah menjalankan semua yang ada pada **1.a Instalasi**, jalankan `npm start`
2. Akses `localhost:3000/`, jika tidak ada error maka akan tampil database yang ada

## 3. Library yang digunakan
* [KnexJS](https://knexjs.org/) - Query builder
* [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) - Library untuk generate jwt
* [md5](https://www.npmjs.com/package/md5`) - Library untuk generate hash password
* [crypto](https://www.npmjs.com/package/crypto) - Library untuk generate id
* [ExpressJS](https://expressjs.com/) - Framework untuk membuat API
* [jest](https://jestjs.io) - Framework untuk melakukan unit test

## 4. Database yang digunakan
* [MariaDB 10.4.22](https://www.postgresql.org/about/news/postgresql-13-released-2077/) - Database SQL

## 5. Endpoint 

### Note: Pastikan anda melakukan login dengan cara memasukkan username dari info username dan password terlebih dahulu.
**Gunakan end point ini dengan cara menggabungkan dengan `localhost:3500`**

url| method | Body | Params | Querystring | Penjelasan
---|---|---|---|---|---
`/login`| POST | `{"username": "username_anda","password": "password dari user anda"}` | - | - | Untuk login 
`/gifts`|GET| - | `?page=<integer> (untuk parameter halaman yang diminta) , ?limit=<integer> (untuk parameter limit per tarikan data,?sort=<string>(masukan Terbaru Untuk sort data terbaru, masukan Lama untuk data lama), ?sort_rating (Masukn terbesar jika ingin mengambil data terbesar dan masukan Terkecil untuk mengambil data terkecil)` | - | Untuk tarikan data semua gifts yang ada, Api ini bisa di pakai oleh semua level user
`/gifts`|GET| - | - | `/:id_gifts` | Untuk mendapatkan detail 1 data gifts,Api ini bisa di pakai oleh semua level user
`/gifts`|POST| [Object](#object-post-gifts) | - | - | Untuk menambahkan data gifts,Api ini bisa di pakai oleh level user admin dan super admin
`/gifts`|PUT| [Object](#object-put-gifts) | - | `/:id_gifts` | Untuk mengubah data,Api ini bisa di pakai oleh level user admin dan super admin
`/gifts`|DELETE| - | - | `/:id_gifts` | Untuk menghapus data gifts ,Api ini bisa di pakai oleh level user admin dan super admin
`/gifts/:id_gifts/redeem `|POST| - | - | `/:id_gifts` | Untuk mereedem pada gifts tertentu untuk user yang telah mereedem, Api ini bisa di pakai oleh semua level user
`/gifts/:id_gifts/rating `|POST| [Object](#object-post-redeem) | - | `/:id_gifts` | Untuk memeberi rating pada gifts tertentu untuk users yang telah memiliki gifts tersebut,  Api ini bisa di pakai oleh semua level user
`/users`|GET| - | `?page=<integer> (untuk parameter halaman yang diminta) , ?limit=<integer> (untuk parameter limit per tarikan data` | - | Untuk tarikan data semua users yang ada, Api ini bisa di pakai oleh semua level super admin
`/users`|POST| [Object](#object-post-users) | - | - | Untuk menambahkan data users,Api ini bisa di pakai oleh level super admin
`/users`|PUT| [Object](#object-put-users) | - | `/:id_users` | Untuk mengubah data,Api ini bisa di pakai oleh level super admin
`/users`|DELETE| - | - | `/:id_users` | Untuk menghapus data users ,Api ini bisa di pakai oleh level super admin
`/register`|POST| [Object](#object-post-register) | - | - | Untuk menambahkan data user 

## Referensi
---
### Object Post Gifts
    {
        "name_gifts": "Namaa",
        "descriptioan_gifts":"Ini adalah contoh Detail",
        "is_available":'1',
        "is_best_seller":'0',
        "is_hot_item":'0',
        "img_gifts": "",
        "stock":20
    }

### Object Put Gifts
    {
        "name_gifts": "Namaa",
        "descriptioan_gifts":"Ini adalah contoh Detail",
        "is_available":'1',
        "is_best_seller":'0',
        "is_hot_item":'0',
        "img_gifts": "",
        "stock":20
    }

### Object Post Rating
    {
        "rating": 2.55
    }

### Object Post User
    {
        "email":"admins@gmail.com",
        "username":"admin",
        "name":"admin",
        "password":"pasword",
        "is_level": 1
    }
### Object Put User
    {
        "email":"admins@gmail.com",
        "username":"admin",
        "name":"admin",
        "password":"pasword",
        "is_level": 1
    }

### Object Post Register
    {
        "email":"users@gmail.com",
        "username":"users",
        "name":"users",
        "password":"users",
    }