const express = require('express')
const router = express.Router();
const userController =  require('../../modules/users/controller/users.controller')
const {auth} = require('../../middelware/auth.middelware');
const {global,admisglobal, superAdminOnly} = require('../../middelware/level.middelware');

router.post('/',userController.register)
module.exports = router;