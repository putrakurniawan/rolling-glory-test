const express = require('express')
const router = express.Router();
const userController =  require('../../modules/users/controller/users.controller')
const {auth} = require('../../middelware/auth.middelware');
const {global,admisglobal, superAdminOnly} = require('../../middelware/level.middelware');
router.get('/', auth,superAdminOnly,userController.users_get);

router.post('/', auth,superAdminOnly,userController.user_post_or_put);
router.put('/:id_users', auth, superAdminOnly,userController.user_post_or_put);
router.delete('/:id_users', auth, superAdminOnly,userController.deletedata);

router.post('/register',userController.register)
module.exports = router;