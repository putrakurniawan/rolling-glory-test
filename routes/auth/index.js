const express = require('express')
const router = express.Router();
const loginController =  require('../../modules/auth/controller/auth.controller')

router.post('/', loginController.login)

module.exports = router;