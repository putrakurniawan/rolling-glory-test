var express = require('express');
var router = express.Router();
const gifts = require('./gifts/index');
const login = require('./auth/index');
const users = require('./users/index');
const register = require('./register/index');

router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.use('/login', login);
router.use('/gifts', gifts);
router.use('/users', users);
router.use('/register', register)

module.exports = router;
