const express = require('express')
const router = express.Router();
const giftsController =  require('../../modules/gifts/controller/gifts.controller')
const {auth} = require('../../middelware/auth.middelware');
const {global,admisglobal} = require('../../middelware/level.middelware');
router.get('/', giftsController.gifts_get);

router.post('/', auth,admisglobal,giftsController.gifts_put_or_post);
router.put('/:id_gifts', auth, admisglobal,giftsController.gifts_put_or_post);
router.patch('/:id_gifts', auth, admisglobal,giftsController.gifts_put_or_post);
router.delete('/:id_gifts', auth, admisglobal,giftsController.deleteData);
router.post('/:id_gifts/:type_act', auth, global, giftsController.insert_or_update_redeem_or_rating);

module.exports = router;