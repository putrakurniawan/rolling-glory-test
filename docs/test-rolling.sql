-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2022 at 03:10 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test-rolling`
--

-- --------------------------------------------------------

--
-- Table structure for table `gifts`
--

CREATE TABLE `gifts` (
  `id` int(11) NOT NULL,
  `id_gifts` varchar(20) NOT NULL,
  `name_gifts` varchar(50) NOT NULL,
  `descriptioan_gifts` text NOT NULL,
  `is_available` enum('0','1') NOT NULL,
  `is_best_seller` enum('0','1') NOT NULL,
  `is_hot_item` enum('0','1') NOT NULL,
  `img_gifts` text NOT NULL,
  `created_by` char(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gifts`
--

INSERT INTO `gifts` (`id`, `id_gifts`, `name_gifts`, `descriptioan_gifts`, `is_available`, `is_best_seller`, `is_hot_item`, `img_gifts`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'RGT-2a78edb743476756', 'Hp', 'deskripsi hp', '1', '0', '0', 'Hp.jpg', '00000001', '2022-03-03 13:03:09', NULL),
(2, 'RGT-710fddc9a922a0d9', 'laptop', 'deskripsi laptop', '1', '0', '0', 'laptop.jpg', '00000001', '2022-03-03 13:03:19', NULL),
(3, 'RGT-575ffd31cb438ec7', 'tab', 'deskripsi tab', '1', '0', '0', 'tab.jpg', '00000001', '2022-03-03 13:03:27', NULL),
(4, 'RGT-48cbd0a5e18046b3', 'tws', 'deskripsi tws', '1', '0', '0', 'tws.jpg', '00000001', '2022-03-03 13:04:07', NULL),
(5, 'RGT-8ac9c51b009f42a5', 'headset', 'deskripsi headset', '1', '0', '0', 'headset.jpg', '00000001', '2022-03-03 13:04:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gifts_ratings`
--

CREATE TABLE `gifts_ratings` (
  `id` int(11) NOT NULL,
  `id_gifts` char(20) NOT NULL,
  `id_users` char(20) NOT NULL,
  `id_rating` char(20) NOT NULL,
  `rating` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gifts_redeem`
--

CREATE TABLE `gifts_redeem` (
  `id` int(11) NOT NULL,
  `id_redeem` char(20) NOT NULL,
  `id_gifts` char(20) NOT NULL,
  `nomor_redeem` char(20) NOT NULL,
  `id_users` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gifts_stock`
--

CREATE TABLE `gifts_stock` (
  `id` int(11) NOT NULL,
  `id_gifts` varchar(20) NOT NULL,
  `id_stock` varchar(20) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gifts_stock`
--

INSERT INTO `gifts_stock` (`id`, `id_gifts`, `id_stock`, `stock`) VALUES
(1, 'RGT-2a78edb743476756', 'ST-5470f4cf167c1f127', 10),
(2, 'RGT-710fddc9a922a0d9', 'ST-c3cb16cf0cb9fcfe9', 10),
(3, 'RGT-575ffd31cb438ec7', 'ST-0b7b820794dd3b916', 10),
(4, 'RGT-48cbd0a5e18046b3', 'ST-64f246592ff607ae4', 10),
(5, 'RGT-8ac9c51b009f42a5', 'ST-e466ba605a221ffe1', 10);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `level` char(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `level`, `created_at`, `updated_at`) VALUES
(1, 'super admin', NULL, NULL),
(2, 'admin', '2022-03-03 13:15:43', '2022-03-03 13:15:43'),
(3, 'user', '2022-03-03 13:15:43', '2022-03-03 13:15:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_users` char(20) NOT NULL,
  `email` char(50) NOT NULL,
  `username` char(50) NOT NULL,
  `name` char(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_level` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_users`, `email`, `username`, `name`, `password`, `is_level`, `created_at`, `updated_at`) VALUES
(1, '00000001', 'superadmin@gmail.com', 'super admin', 'super admin', '5ab08155a29c77daaaac427bcad24769', 1, '2022-03-01 06:50:52', '2022-03-01 06:50:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gifts`
--
ALTER TABLE `gifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_gifts` (`id_gifts`);

--
-- Indexes for table `gifts_ratings`
--
ALTER TABLE `gifts_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gifts_redeem`
--
ALTER TABLE `gifts_redeem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gifts_stock`
--
ALTER TABLE `gifts_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_gifts` (`id_gifts`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_users` (`id_users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gifts`
--
ALTER TABLE `gifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gifts_ratings`
--
ALTER TABLE `gifts_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gifts_redeem`
--
ALTER TABLE `gifts_redeem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gifts_stock`
--
ALTER TABLE `gifts_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gifts_stock`
--
ALTER TABLE `gifts_stock`
  ADD CONSTRAINT `gifts_stock_ibfk_1` FOREIGN KEY (`id_gifts`) REFERENCES `gifts` (`id_gifts`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
