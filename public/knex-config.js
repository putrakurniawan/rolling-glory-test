require('dotenv').config();
const ObjConn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
}

const DB = require('knex')({
    client: 'mysql',
    connection: ObjConn,

    searchPath: ['test_rolling'],
    pool: {
        min: 0,
        max: 10,
    }
});

module.exports = DB