const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const fs = require('fs');
const app = express();
require('dotenv').config();

app.use(bodyParser.json({
    limit: '1mb',
    extended: false,
    strict: false
}));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.raw());


let httpServer;
app.use('/',require('../routes/index'));

httpServer = http.createServer(app);
runServer();
function runServer() {
    httpServer.listen(process.env.PORT_SERVER, function(req, res) {
        let port = httpServer.address().port;
        try {
            console.log("SERVER listening @ port: ", port);
        } catch (err) {
            if (err.code != 'EEXIST') {
                throw err;
            }
        }
    });
}