let methods = {};

methods.returnend = async function (res, data = null) {
    res.setHeader("Content-Type", "application/json;charset=utf-8");

    if (data != null) {
        res.status(data['response_code']).json({
            "response_code":data['response_code'],
            "message": data['message'],
            "status": data['status'],
            "data": data['data'],

        });
    }
}

module.exports = methods;